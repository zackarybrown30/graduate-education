This is likely your first interaction with BitBucket.

I think youll be thrilled to learn that its nearly identical to GitHub and uses the same commands! it just has much better software for version control.
Our goal here is for you to implement Version Control, learn to fork,  and to log your own Branch based on this BitBucket account to get practice emulating real world experience. Hopefully in time we will implement Jira and learn AGILE concepts
For now you will need to become a collaborator, then you should create your own branch with your first name being the branch title. 
We will go over this if you have not done this before. -Zackary
*NOTE: is forking a better solution for this? lets discuss, then adjust this 

WEEK ONE: 

Monday: Feb 20
Heaps, We will learn what a heap is, understand Domain Knowledge of heap.
When do you use it, Why would you use it? What are its strengths and weaknesses?
* Most impportantly * If i were a computer how would i think when engaging heaps?

Tuesday: Feb 21
TBD Ideas, 
Github, Bitbucket, and version control and AGILE
Protoype functions and Converting whiteboard exercises to executable Javascript code.
DISCUSSION: Which data structure will we study tomorrow, Briefly overview it.

Wednesday: Feb 22
Data Structure # 2 Decided Tuesday.

Thursday: Feb 23
Interview Engagement Practice, 
Show me your resumes and I will interview you on your material. It doesn't have to be complete. In fact if youre working on it its best becasue i will ensure its oriented to be a talking point for you. 

Friday: NETWORKING 101,
If you don't have a coffee meeting or beer with someone setup for next week, we will make sure it happens!